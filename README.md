# MhhModels

[![CI Status](http://img.shields.io/travis/Mathias H. Hansen/MhhModels.svg?style=flat)](https://travis-ci.org/Mathias H. Hansen/MhhModels)
[![Version](https://img.shields.io/cocoapods/v/MhhModels.svg?style=flat)](http://cocoapods.org/pods/MhhModels)
[![License](https://img.shields.io/cocoapods/l/MhhModels.svg?style=flat)](http://cocoapods.org/pods/MhhModels)
[![Platform](https://img.shields.io/cocoapods/p/MhhModels.svg?style=flat)](http://cocoapods.org/pods/MhhModels)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MhhModels is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MhhModels'
```

## Author

Mathias H. Hansen, dev@mathiashh.dk

## License

MhhModels is available under the MIT license. See the LICENSE file for more info.
