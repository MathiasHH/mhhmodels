//  
//  AuthModelImpl.swift
//  MhhModels
//
//  Created by Mathias H. Hansen on 18/01/2018.
//  Copyright © 2018 Mathias H. Hansen. All rights reserved.
//
//  Copyright 2017-2018 Mathias Hedemann Hansen.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import MhhCommon


public struct AuthModelImpl: AuthModel {

	// MARK: Properties
	
	public let authNetwork: AuthNetwork
	public var rules: AuthRules?

	
	// MARK: Init
	
	public init(authNW: AuthNetwork, authRules: AuthRules) {
		self.authNetwork = authNW
		rules = authRules 
	}

	
	// MARK: Auth protocol conformation
	
	public func login(
		with email: String,
		and password: String,
		callback: @escaping (Error?, User?) -> Void
		) {
		if let rules = rules {
			if !rules.isPasswordValid(password) {
				print("is valid password: false)")
				return
			}
		}
		
		authNetwork.login(with: email, and: password) { (error: Error?, user: User?) in
			if let user = user {
				// TODO: mhh. Maybe move this to another rules class
				if !user.isEmailVerified {
					// if email is not verified we dont want the user to be logged in
					self.logout(callback: nil)
					// A user must have verified his email before he can log in. so send back error
					callback(AuthError.emailNotVerified, nil)
					return
				}
			}
	
			callback(error, user)
		}
	}
	
	public func logout(callback: ((Error?, Bool) -> Void)?) {
		authNetwork.logout(callback: callback)
	}
	
	public func resetEmail(email: String, callback: ((Error?) -> Void)? = nil) {
		authNetwork.resetEmail(email: email, callback: callback)
	}
	
	
	// MARK: AuthModel protocol conformation
	
	public func tryToRegisterUser(
		with email: String,
		and password: String,
		and confirmPassword: String,
		callback: @escaping (Error?, Any?) -> Void) {

		if let rules = rules {
	
			if !rules.isPasswordEqualToConfirmPassword(password: password, confirmPassword: confirmPassword) {
				print("password and confirm password are not the same")
				return
			}
			
			if !rules.isPasswordValid(password) {
			  print("is valid password: false)")
			  return
		  }
		}
		createUser(with: email, and: password, callback: callback)
	}

	public func sendVerificationEmail(
		email: String,
		password: String,
		callback: @escaping (Error?, User?) -> Void
	) {
		authNetwork.login(with: email, and: password) { (error: Error?, user: User?) in
			if let error = error {
				print("Model. Error \(error)")
  			callback(error, user)
			}
			if let user = user {
				let (error2, user2) = self.emailVerificationLogic(user)
				callback(error2, user2)
			}
		}
	}
	
	public func tryToChangeEmail(
		currentEmail: String,
		newEmail: String,
		password: String,
		callback: (Error?) -> Void
	) {
		assertionFailure("Mhh says: This is not implented yet")
		// TODO: mhh. implement this functionality
		// TODO: mhh.
		// rules:
		// 1) The user input: currentEmail, newEmail and password must be filled
		// 2) The user input currentEmail and newEmail cannot be the same
		// 3) Try to login. If success do 4
		// 4) Try to change email address on server
		// 5) show error or success back to the user
	}
	
	// MARK: Private API
	
	// TODO: THere should also be a func like:
	// public func createUser(with email: String, and password: String, db:Database, callback: @escaping (Error?, Any?, Any?) -> Void) // the last any should be used to send the auth back from the network/firebase. The database should be used to save that a user is logged in.
	// TODO: change with email: String, and password: String to user: User
	private func createUser(with email: String, and password: String, callback: @escaping (Error?, User?) -> Void) {
		authNetwork.createUser(
			with: email,
			and: password
		) { (error, user) in
			if let error = error {
				print("model. my error \(error) not created")
			}
			if let user = user {
				print("model. my user \(user) created")
				let (error2, user2) = self.emailVerificationLogic(user)
				callback(error2, user2)
				return
			}
			callback(error, user)
		}
	}

	private func sendVerificationEmail(user: User) {
		authNetwork.sendVerificationEmail(user: user)
	}
	
	// TODO: mhh. Maybe move this to another rules class
	fileprivate func emailVerificationLogic(_ user: User) -> (Error?, User?) {
		// Firebase by default login a new user. We want the user to verify his email before he can be logged in so we sign out the user
		self.logout(callback: nil)
		
		if user.isEmailVerified {
			return (AuthError.emailAlreadyVerified, nil)
		} else {
			self.sendVerificationEmail(user: user)
			return (nil, user)
		}
	}


}

