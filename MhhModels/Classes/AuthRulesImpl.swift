//  
//  AuthRulesImpl.swift
//  MhhModels
//
//  Created by Mathias H. Hansen on 27/01/2018.
//
//  Copyright 2017-2018 Mathias Hedemann Hansen.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//


import MhhCommon

// TODO: Here there should be some middleware injected. Some login rules.
// password and confirm password should match. DONE
// the password must not be empty string.
// the password should be in some specific form. Like longer than 6, ect.
// email should be a valid email
// the rules should have a function to check that only some emails are valid. like som company email. Eg. somename@mathiashh.dk
// Maybe use builder pattern for the rules
public struct AuthRulesImpl: AuthRules {
	
	public init() {}
	
	public func isPasswordEqualToConfirmPassword(password: String, confirmPassword: String) -> Bool {
		return password == confirmPassword
	}
	
	public func isPasswordValid(_ password: String) -> Bool {
//		return password.contains("123456")
		return true // TODO: mhh. Implement rules
	}

}

//public struct SignupRulesImpl: SignupRules {
//
//	public init() {}
//
//}

