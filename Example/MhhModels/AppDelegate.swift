//
//  AppDelegate.swift
//  MhhModels
//
//  Created by Mathias H. Hansen on 01/24/2018.
//  Copyright (c) 2018 Mathias H. Hansen. All rights reserved.
//

import UIKit
import MhhModels
import MhhCommon

//struct AuthStub: NotUIAuth {
//	func createUser(with email: String, and password: String, callback: @escaping (Error?, Any?) -> Void) {
//	}
//
//	func login(with email: String, and password: String, callback: @escaping (Error?, Any?) -> Void) {
//	}
//
//	func sendVerificationEmail(email: String, password: String) {
//	}
//
//	func resetEmail(email: String) {
//	}
//
//	func logout(callback: ((Error?, Bool) -> Void)?) {
//	}
//
//
//
//}
struct authNWStub: AuthNetwork {
	func createUser(with email: String, and password: String, callback: @escaping (Error?, User?) -> Void) {
	}
	
	func sendVerificationEmail(user: User) {
	}
	
	func changeEmail(newEmail: String, callback: (Error?) -> Void) {
	}
	
	func login(with email: String, and password: String, callback: @escaping (Error?, User?) -> Void) {
	}
	
	func logout(callback: ((Error?, Bool) -> Void)?) {
	}
	
	func resetEmail(email: String, callback: ((Error?) -> Void)?) {
	}
	
	
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		let stub = authNWStub()
		let rules = AuthRulesImpl()
		let auth = AuthModelImpl(authNW: stub, authRules: rules)
		auth.login(with: "hej@meddig.dk", and: "lortpaadaase") { (error, user) in
			if let user = user {
				print("my user \(user)")
			}
			if let error = error {
				print("my error \(error)")
			}
		}
		
		return true
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	
}

