#
# Be sure to run `pod lib lint MhhModels.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MhhModels'
  s.version          = '0.1.0'
  s.summary          = 'MhhModels is a library for common logic.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'MhhModels is a library for common logic. The purpose is to not write the same code all the time. Like calls to the network, calls to the database, common business logic ect.'

  s.homepage         = 'https://bitbucket.org/MathiasHH/MhhModels'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Mathias H. Hansen' => 'dev@mathiashh.dk' }
  s.source           = { :git => 'https://bitbucket.org/MathiasHH/MhhModels.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.source_files = 'MhhModels/Classes/**/*'
  
  s.dependency 'MhhCommon'
end
